# DIO_DevOps_BootCamp

Script Bash para criar usuários, dar permissões em suas respectivas pastas e acesso total na pasta pública.

Orientação do projeto:
- Todo provisionamento deve ser feito em um arquivo tipo Bash Script
- O dono de todos os diretórios criados será o usuário root
- Os usuários de cada grupo terão permissão total dentro de seu respectivo diretório
- Os usuários não poderão ter permissão de leitura, escrita e execução em diretórios de departamentos que eles não pertencem
- Subir o arquivo de script criado para a conta do GitHub | GitLab